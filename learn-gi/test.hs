{-# LANGUAGE OverloadedStrings, OverloadedLabels #-}

import qualified GI.Gtk as Gtk
import Data.GI.Base

main :: IO ()
main = do
  Gtk.init Nothing
  window <- new Gtk.Window [ #title := "Hi there" ]
  on window #destroy Gtk.mainQuit

  grid <- Gtk.gridNew
  #add window grid

  da <- Gtk.drawingAreaNew
  Gtk.widgetSetSizeRequest da 256 256
  Gtk.gridAttach grid da 0 0 1 1

  button <- new Gtk.Button [ #label := "Click me" ]
  on button #clicked (set button [ #sensitive := False, #label := "Thanks for clicking me" ])
  Gtk.gridAttach grid button 0 1 1 1
  -- #add win button

  quit <- new Gtk.Button [ #label := "Exit" ]
  on quit #clicked Gtk.mainQuit
  Gtk.gridAttach grid quit 1 1 1 1

  #showAll window
  Gtk.main
  
