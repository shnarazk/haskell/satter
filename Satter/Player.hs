-- | Define the behaviors of 'Player'
--
-- TODO
--
{-# LANGUAGE
	GADTs
      , MultiParamTypeClasses
      , ScopedTypeVariables
      , ViewPatterns
  #-}
module Satter.Player
       (
         nearestPlayers
       ) where

import Control.Applicative
import Control.Lens
import Control.Monad
import Data.List
import Data.Ord
import Satter.TwoD
import Satter.Field
import Satter.Types
import Satter.Ball
import Satter.PlayerSAT

dribbleSpeed, movingSpeed, kickSpeed :: Double
dribbleSpeed = 1.25
movingSpeed = 1.3
kickSpeed = 2

dribble :: ActionTarget -> Intention
dribble = DribblingTo dribbleSpeed

-- | call updateIntention to set new mental image, then check the return value
safeUpdateIntention :: GameContext -> Player -> IO Player
safeUpdateIntention c@(_ball -> b) p = (c <!> mb) p
  where mb = if near2 (_velocity $ geometricsOf p) (positionOf p) (positionOf b) then Just b else Nothing

-- | new interface for updating mental space
(<!>) :: GameContext -> Maybe Ball -> Player -> IO Player
(<!>) c b p = (p &) . (intention .~) <$> newIntention c b p

-- | call updateGeometrics to set new positions, then check the return value
safeUpdatePhysics :: GameContext -> Player -> IO (Either (Player, Ball) Player)
safeUpdatePhysics c@(_ball ->b) p@(geometricsOf -> g)
  | dropLength prePos (positionOf b) (positionOf p) < catchable && (_holdBy b == Nothing || _holdBy b == Just idNum) = do
    let b' = b & geometrics_b .~ geometricsOf p
    res <- ((c & ball .~ b') >< Just b') p
    case res of
      Left (g1, b'') -> return $ Left (p &~ do geometrics_p .= up g g1, b'')
      Right g1 -> return . Right $ p & geometrics_p .~ up g g1
  | near2 catchable (positionOf b) (positionOf p) && (_holdBy b == Nothing || _holdBy b == Just idNum) = do
    let b' = b & geometrics_b .~ geometricsOf p
    res <- ((c & ball .~ b') >< Just b') p
    case res of
      Left (g1, b'') -> return $ Left (p &~ do geometrics_p .= g + g1, b'')
      Right g1 -> return . Right $ p & geometrics_p .~ up g g1
  | otherwise = do
    res <- Right . either fst id <$> (c >< Nothing) p
    case res of
      Left _ -> error "illegal return type"
      Right g1 -> return . Right $ p & geometrics_p .~ up g g1
  where
    inertia = 0.5
    up' x y = inertia * x + (1 - inertia) * y
    up (Geometrics (x1, y1) (dx1, dy1) v1) (Geometrics (x2, y2) (dx2, dy2) v2) =
      Geometrics (up' x1 x2, up' y1 y2) (up' dx1 dx2, up' dy1 dy2) (up' v1 v2)
    catchable = 2.6 * ballSize
    idNum = p ^. (property . idNumber)
    prePos = (b <!! (-1)) ^. (geometrics_b . position)
    -- | 線分に対する垂線の長さ。これは TwoD.dropLength とは定義が違うので注意
    dropLength (x1, y1) (x2, y2) (x, y)
      | x1 == x2 = if (y1 - y) * (y2 - y) < 0 then abs (x1 - x) else 10000
      | (x1 - x) * (x2 - x) <= 0 = abs (a * x + b * y + c) / sqrt (a ** 2 + b ** 2)
      | otherwise = 10000
      where
        a = y2 - y1
        b = x1 - x2
        c = (x2 - x1) * y1 - x1 * (y2 - y1)

-- | new interface for updating real world
(><) :: GameContext -> Maybe Ball -> Player -> IO (Either (Geometrics, Ball) Geometrics)
(><) c Nothing p = Right . either fst id <$> updateGeometrics c Nothing p
(><) c b@(Just _) p = updateGeometrics c b p

-- | obsolete now!
newPosition :: Geometrics -> Double -> TwoD
newPosition (Geometrics (x, y) (dx, dy) v) t =(x + dx * v * t * simulationScale, y + dy * v * t * simulationScale)

-- | better function for determining the next position from 'pos' 'dir' and v
newGeometrics :: TwoD -> TwoD -> Double -> Geometrics
newGeometrics (x, y) (dx, dy) v = Geometrics (x + dx * v * simulationScale, y + dy * v * simulationScale) (dx, dy) v

intentionDecay :: Double
intentionDecay = 0.09

keepIntension :: Intention -> Intention
keepIntension (Lost d) = Lost (max 0 (d - intentionDecay))
keepIntension (GoFor d t) = GoFor (max 0 (d - intentionDecay)) t
keepIntension (KickTo d t) = KickTo (max 0 (d - intentionDecay)) t
keepIntension (DribblingTo d t) = DribblingTo (max 0 (d - intentionDecay)) t
keepIntension i = i

instance SObject Player where
  geometricsOf = _geometrics_p
  sizeOf _ = playerSize
  updateIntention = safeUpdateIntention
  updatePhysics = safeUpdatePhysics
  (<!!) a@(Player pg _ _) t = a & geometrics_p . position .~ newPosition pg t

--------------------------------------------------------------------------------

nearestPlayers :: TwoD -> [(PlayerProperty, TwoD)] -> [PlayerProperty]
nearestPlayers (x, y) assoc = map fst $ sortBy (comparing (dist . snd)) assoc
  where dist (x1, y1) = sqrt $ (x1 -x) ** 2 + (y1 - y) ** 2

-- | short to long
sortByDistance :: TwoD -> [(PlayerProperty, TwoD)] -> [(PlayerProperty, TwoD)]
sortByDistance (x, y) assoc = sortBy (comparing (dist . snd)) assoc
  where dist (x1, y1) = sqrt $ (x1 -x) ** 2 + (y1 - y) ** 2


moveToSpareSpace :: GameContext -> Player -> IO (Maybe TwoD)
moveToSpareSpace game (Player (_position -> (x, y)) (PlayerProperty _ t _ index _) _) = do
  let poss = (map snd . filter ((index /=) . _idNumber . fst) . (if t == Team1 then fst else snd)) $ getPositions game
  let dist (x1, y1) = sqrt $ (x1 -x) ** 2 + (y1 - y) ** 2
  let nearest = minimumBy (comparing dist) poss
  if near2 200 nearest (x, y)
    then return . Just $ directionTo nearest (x, y)
    else return Nothing

-- | 自分より前にいる最近接敵がdより近ければTrueを返す
enemyIsNear :: GameContext -> Player -> Double -> IO Bool
enemyIsNear game (Player (_position -> (x, y)) (PlayerProperty _ t _ _idNumber _) _) d = do
  let poss = (map snd . (if t == Team1 then snd else fst)) $ getPositions game
  let closerToGoal (_, y') = if t == Team1 then y' < y else y < y'
  let dist (x1, y1) = sqrt $ (x1 -x) ** 2 + (y1 - y) ** 2
  case filter closerToGoal poss of
    [] -> return False
    l -> let nearest = minimumBy (comparing dist) l in return $ near2 d nearest (x, y)

--------------------------------------------------------------------------------

newIntention :: GameContext -> Maybe Ball -> Player -> IO Intention

newIntention g b p@(Player _ (_roll -> PlayerSAT) _) = do
  -- print $ makeLiterals g b p
  newIntentionBySAT g b p

--------------------------------------------------------------------------------
-- | Lost Mind
-- newIntention _ (Just _) (_intention -> int@(Lost 0)) = return $ KickTo 0.2 TargetGoal
newIntention _ _ (_intention -> Lost 0) = return NoPlan
newIntention _ _ (_intention -> int@(Lost d)) = return $ keepIntension int

--------------------------------------------------------------------------------
-- | No plan
-- | NoPlan with ball
newIntention g Nothing p@(Player (_position -> pos) (PlayerProperty PlayerDefence _ _ i _) NoPlan) = do
  let b@(Ball _ ballKeeper) = _ball g
  let tc = getTeamContext g p
  let aggresive = _directorIntention tc == AggressiveAttack
  let t = max 1 $ expectedTime b (positionOf p) (p ^. geometrics_p . velocity)
  let nears = (map _idNumber . nearestPlayers (positionOf (b <!! t))) $ getTeammatePositions g p False
  let enemyBall = case g ^. ball . holdBy of { Nothing -> False; Just x -> notElem x (_teammates tc) }
  case () of
    _ | ballKeeper == Nothing && head nears == i && near2 200 pos (positionOf b) -> return $ GoFor 0.85 TargetBall
    _ | not aggresive && ballKeeper == Nothing && elem i (take 2 nears) -> return $ GoFor 0.8 TargetBall
    _ | head nears == i -> return $ GoFor 0.8 TargetBall
--    _ | isInOurArea g p (positionOf b) && inEnemyArea g p -> return $ GoFor 0.8 TargetDefaultPosition
--    _ | enemyBall -> return $ GoFor 0.6 TargetDefaultPosition
    _ | isInEnemyArea g p (positionOf b) -> return $ GoFor 0.2 TargetOffsideLine
    _ | isInOurArea g p (positionOf b) -> return $ GoFor 0.2 TargetBall
    _ | isInOurPenaltyArea g p (positionOf b) -> return $ GoFor 0.4 TargetBall
--    _ | isInOurArea g p (positionOf b) || not (near2 500 pos (positionOf b)) -> return $ GoFor 0.1 TargetBall
--    _ | 100 < pos <-> (p ^. property . defaultPosition) -> return $ GoFor 0.1 TargetDefaultPosition
    _ | positionOf (nthPlayer g (head nears)) <-> pos < 20 -> return $ GoFor 0.4 TargetDefaultPosition
    _ -> return $ GoFor 0.25 TargetBall
--    _ -> return $ GoFor 0.25 TargetDefaultPosition
--    _ -> return NoPlan

newIntention g Nothing p@(Player (_position -> pos) (_roll -> PlayerKeeper) NoPlan) = do
  let tc = getTeamContext g p
  let aggresive = _directorIntention tc == AggressiveAttack
  let b = _ball g
  case () of
    _ | near2 120 pos (positionOf b) -> return $ GoFor 0.85 TargetBall
    _ | near2 250 pos (positionOf b) -> return $ GoFor 0.7 TargetBall
    _ | isInOurArea g p (positionOf b) -> return $ GoFor 0.1 TargetBall
    _ | aggresive -> return $ GoFor 0.1 TargetBall
    _ -> return $ GoFor 0.8 TargetBall

newIntention g Nothing p@(Player (_position -> pos) (_idNumber -> i) NoPlan) = do
  let b@(Ball _ ballKeeper) = _ball g
  let tc = getTeamContext g p
  let aggresive = _directorIntention tc == AggressiveAttack
  let nears = (map _idNumber . nearestPlayers (positionOf b)) $ getTeammatePositions g p False
  let beyondOurOffside = beyond g p TargetOffsideLine
  let ourBall = case ballKeeper of { Nothing -> False; Just x -> elem x (_teammates tc) }
  let myID = p ^. (property . idNumber)
  case () of
    _ | ballKeeper == Nothing && not ourBall && near2 100 pos (positionOf b) && elem i (take 2 nears) -> return $ GoFor 0.8 TargetBall
    _ | not aggresive && not ourBall && elem i (take 2 nears) && near2 200 pos (positionOf b) -> return $ GoFor 0.65 TargetBall
    _ | ourBall && p ^. property . roll == PlayerForward -> return $ GoFor 1.2 TargetOffsideLine
    _ | head nears == i -> return $ GoFor 0.8 TargetBall
    _ | beyondOurOffside -> return $ GoFor 0.8 DefenceLine
    _ | not (elem myID (take 3 nears)) {- && not (near2 200 (positionOf p) (p ^. property . defaultPosition)) -} ->
      return $ GoFor 0.7 TargetDefaultPosition
    _ | positionOf (nthPlayer g (head nears)) <-> pos < 20 -> return $ GoFor 0.4 TargetDefaultPosition
    _  -> return $ GoFor 0.15 TargetBall
--    _  -> return NoPlan

newIntention g (Just _) p@(Player (_position -> pos) (_idNumber -> i) (GoFor _ TargetOffsideLine)) = return NoPlan

newIntention g Nothing p@(Player (_position -> pos) (_idNumber -> i) (GoFor _ TargetOffsideLine)) = do
  let b@(Ball _ ballKeeper) = _ball g
  let tc = getTeamContext g p
  let aggresive = _directorIntention tc == AggressiveAttack
  let nears = (map _idNumber . nearestPlayers (positionOf b)) $ getTeammatePositions g p False
  let beyondOurOffside = beyond g p TargetOffsideLine
  let ourBall = case ballKeeper of { Nothing -> False; Just x -> elem x (_teammates tc) }
  let myID = p ^. (property . idNumber)
  case () of
    _ | ballKeeper == Nothing && not ourBall && near2 100 pos (positionOf b) -> return $ GoFor 0.8 TargetBall
    _ | not aggresive && not ourBall && elem i (take 2 nears) && near2 200 pos (positionOf b) -> return $ GoFor 0.65 TargetBall
    _ | ourBall && p ^. property . roll == PlayerForward -> return $ GoFor 1.2 TargetOffsideLine
    _ | not (elem myID (take 3 nears)) && not (near2 200 (positionOf p) (p ^. property . defaultPosition)) ->
      return $ GoFor 0.7 TargetDefaultPosition
    _ | head nears == i -> return $ GoFor 0.8 TargetBall
    _ | beyondOurOffside -> return $ GoFor 0.8 DefenceLine
    _  -> return $ GoFor 0.15 TargetBall
--    _  -> return NoPlan


-- | No plan but with ball
newIntention g (Just b) p@(Player _ (_roll -> PlayerForward) NoPlan) = do
  let tc = getTeamContext g p
  let aggresive = _directorIntention tc == AggressiveAttack
  let nears = (length . filter (near2 40 (positionOf b)) . map snd) $ getEnemyPositions g p
  case () of
    _ | 3 < nears && aggresive -> return $ KickTo 2.0 TargetMate
    _ | 2 < nears -> return $ KickTo 2.0 TargetMate
    _ -> return $ dribble TargetGoal

newIntention _ (Just _) (Player _ (_roll -> PlayerDefence) NoPlan) = return $ KickTo 2.0 TargetMate
newIntention _ (Just _) (Player _ (_roll -> PlayerKeeper) NoPlan) = return $ KickTo 2.3 TargetMate

--------------------------------------------------------------------------------
-- | Go for Ball
newIntention g Nothing p@(Player _ (_roll -> PlayerKeeper) int@(GoFor _ TargetBall)) = do
  let tc = getTeamContext g p
  let b@(Ball _ ballKeeper) = _ball g
  let enemyBall = case g ^. ball . holdBy of { Nothing -> False; Just x -> notElem x (_teammates tc) }
  case () of
   _ | tc ^. directorIntention == AggressiveAttack -> return int
   _ | not (inMyGoalZone p) -> return $ GoFor 1 TargetDefaultPosition
   _  -> return int

newIntention g Nothing p@(Player _ (_roll -> PlayerDefence) int@(GoFor _ TargetBall)) = do
  let tc = getTeamContext g p
  let b@(Ball _ ballKeeper) = _ball g
  let t = max 1 $ expectedTime b (positionOf p) (p ^. geometrics_p . velocity)
  let nears = (map _idNumber . nearestPlayers (positionOf (b <!! t))) $ getTeammatePositions g p False
  let ourBall = case ballKeeper of { Nothing -> False; Just x -> elem x (_teammates tc) }
  let enemyBall = case g ^. ball . holdBy of { Nothing -> False; Just x -> notElem x (_teammates tc) }
  let myID = p ^. (property . idNumber)
  let myPos = positionOf p
  case () of
    _ | head nears == myID && not ourBall && elem myID (take 2 nears) -> return $ GoFor 2 TargetBall
    _ | positionOf (nthPlayer g (head nears)) <-> myPos < 10 -> return $ GoFor 0.4 TargetDefaultPosition
    _ | elem myID (take 2 nears) && not ourBall -> return $ GoFor 2 TargetBall
    _ | notElem myID (take 3 nears) && not (near2 200 myPos (p ^. property . defaultPosition)) ->
      return $ GoFor 0.8 TargetDefaultPosition
    _ | notElem myID (take 3 nears) && isInOurArea g p (positionOf b) -> return $ GoFor 0.8 TargetDefaultPosition
    _ | notElem myID (take 7 nears) && isInEnemyArea g p (positionOf b) -> return $ GoFor 0.8 TargetOffsideLine
    _ | not ourBall && near2 80 myPos (positionOf b) -> return $ GoFor 1.0 TargetBall
    _ | enemyBall && isInEnemyArea g p (positionOf b) -> return $ GoFor 1.0 TargetDefaultPosition
    _ | not ourBall && near2 180 myPos (positionOf b) -> return int
    _ -> return int
--    _ -> return NoPlan

newIntention g Nothing p@(_intention -> int@(GoFor _ TargetBall)) = do
  let tc = getTeamContext g p
  let b@(Ball _ ballKeeper) = _ball g
  let t = max 1 $ expectedTime b (positionOf p) (p ^. geometrics_p . velocity)
  let nears = (map _idNumber . nearestPlayers (positionOf (b <!! t))) $ getTeammatePositions g p False
  let ourBall = case ballKeeper of { Nothing -> False; Just x -> elem x (_teammates tc) }
  let enemyBall = case g ^. ball . holdBy of { Nothing -> False; Just x -> notElem x (_teammates tc) }
  let myID = p ^. (property . idNumber)
  let myPos = positionOf p
  case () of
    _ | head nears == myID && not ourBall -> return $ GoFor 2 TargetBall
    _ | positionOf (nthPlayer g (head nears)) <-> myPos < 10 -> return $ GoFor 0.4 TargetDefaultPosition
    _ | ourBall && notElem myID (take 2 nears) -> return $ GoFor 1.2 TargetOffsideLine
    _ | not (elem myID (take 2 nears)) && not (near2 200 (positionOf p) (p ^. property . defaultPosition)) ->
      return $ GoFor 0.8 TargetDefaultPosition
    _ | (p ^. property . roll == PlayerForward) && notElem myID (take 2 nears) -> return $ GoFor 1.2 TargetOffsideLine
    _ | not ourBall && near2 80 (positionOf p) (positionOf b) -> return $ GoFor 1.0 TargetBall
    _ | not ourBall && near2 180 (positionOf p) (positionOf b) -> return int
    _ | not ourBall && near2 340 (positionOf p) (positionOf b) -> return int
    _ -> return int
--    _ -> return NoPlan

newIntention _ (Just _) (Player _ (_roll -> PlayerKeeper) (GoFor _ TargetBall)) = return $ KickTo 2.3 TargetMate

newIntention g (Just b) p@(_intention -> GoFor _ TargetBall) = do
  let tc = getTeamContext g p
  -- let enemyPenaltyArea = inEnemyPenaltyArea g p
  let ourPenaltyArea = inOurPenaltyArea g p
  -- let ourArea = inOurArea g p
  let nears = (length . filter (near2 40 (positionOf b)) . map snd) $ getEnemyPositions g p
  case () of
    _ | 2 < nears -> return $ KickTo 2.2 TargetMate
    _ | ourPenaltyArea -> return $ KickTo 2.2 TargetMate
    _ -> return $ dribble TargetGoal

newIntention g _ p@(Player _ (_roll -> PlayerDefence) int@(GoFor d TargetDefaultPosition)) = do
  let d = movingSpeed
  let tc = getTeamContext g p
  let b = g ^. ball
  let dist = positionOf p <-> (p ^. property . defaultPosition)
  let enemyArea = isInEnemyArea g p (positionOf b)
  let ourArea = isInOurArea g p (positionOf b)
  let enemyBall = case g ^. ball . holdBy of { Nothing -> False; Just x -> notElem x (_teammates tc) }
  let t = max 1 $ expectedTime b (positionOf p) (p ^. geometrics_p . velocity)
  let nears = (map _idNumber . nearestPlayers (positionOf (b <!! t))) $ getTeammatePositions g p False
  let myID = p ^. (property . idNumber)
  case () of
    _ | head nears == myID -> return $ GoFor 0.7 TargetBall
    _ | enemyBall && elem myID (take 2 nears) -> return $ GoFor 0.8 TargetBall
--    _ | dist < 2 * playerSize -> return NoPlan -- $ GoFor 0.1 TargetBall
    _ | enemyArea && enemyArea -> return NoPlan
    _ -> return int

newIntention g _ p@(Player _ (_roll -> PlayerKeeper) int@(GoFor _ TargetDefaultPosition)) = do
  let tc = getTeamContext g p
  let b = g ^. ball
  let dist = positionOf p <-> (p ^. property . defaultPosition)
  let t = max 1 $ expectedTime b (positionOf p) (p ^. geometrics_p . velocity)
  let nears = (map _idNumber . nearestPlayers (positionOf (b <!! t))) $ getTeammatePositions g p False
  case () of
    _ | head nears == p ^. property . idNumber -> return $ GoFor 0.6 TargetBall
    _ | dist < 4 * playerSize -> return NoPlan
    _ -> return int

newIntention g _ p@(Player _ _ int@(GoFor _ TargetDefaultPosition)) = do
  let tc = getTeamContext g p
  let b = g ^. ball
  let dist = positionOf p <-> (p ^. property . defaultPosition)
  let enemyArea = isInEnemyArea g p (positionOf (_ball g))
  let ourArea = isInOurArea g p (positionOf (_ball g))
  let enemyBall = case g ^. ball . holdBy of { Nothing -> False; Just x -> notElem x (_teammates tc) }
  let t = max 1 $ expectedTime b (positionOf p) (p ^. geometrics_p . velocity)
  let nears = (map _idNumber . nearestPlayers (positionOf (b <!! t))) $ getTeammatePositions g p False
  let myID = p ^. (property . idNumber)
  case () of
    _ | head nears == myID -> return $ GoFor 0.8 TargetBall
    _ | enemyBall && elem myID (take 2 nears) -> return $ GoFor 0.7 TargetBall
    _ | enemyArea -> return $ GoFor 0.4 TargetBall
    _ | dist < 8 * playerSize -> return NoPlan
    _ -> return int

--------------------------------------------------------------------------------
-- | Super Base Case of Dribble (for future extension)
newIntention _ Nothing (_intention -> DribblingTo _ _) = return (Lost 3)

--------------------------------------------------------------------------------
-- | Dribble to Goal with ball
newIntention g (Just _) p@(Player _ (_roll -> PlayerDefence) int@(DribblingTo _ TargetGoal)) = do
  let
    tc = getTeamContext g p
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (20, 40)
    goalPos = tc ^. goalPosition
    dist = positionOf p <-> goalPos
  let toGoal = map (_idNumber . fst) . sortByDistance goalPos $ getTeammatePositions g p False
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  case () of
    _ | near2 180 (positionOf p) goalPos -> return $ KickTo (if 250 < dist then 2.6 else 2.4) TargetOffsideLine
    _ | verycloseEnemy && aggresive -> return $ KickTo (if 250 < dist then 2.6 else 2.3) TargetGoal
    _ | verycloseEnemy && inEnemyPenaltyArea g p && head toGoal == (p ^. property . idNumber) -> return $ KickTo 2 TargetGoal
    _ | inEnemyPenaltyArea g p && length (filter (p `isBehind`) (getEnemies g p)) <= 4 -> return $ KickTo 2.3 TargetGoal
    _ | inEnemyArea g p && length (filter (p `isBehind`) (getEnemies g p)) <= 5 -> return $ int
    _ | verycloseEnemy -> return $ KickTo 1.7 TargetOffsideLine
    _ | closeEnemy && not aggresive -> return $ KickTo 2.2 TargetMate
    _ -> return int

newIntention g (Just _) p@(Player (_position -> pos) _ int@(DribblingTo _ TargetGoal)) = do
  let
    tc = getTeamContext g p
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (20, 40)
    goalPos = tc ^. goalPosition
  let toGoal = map (_idNumber . fst) . sortByDistance goalPos $ getTeammatePositions g p False
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  case () of
    _ | aggresive && near2 200 pos goalPos -> return $ KickTo 2.3 TargetGoal
    _ | near2 160 pos goalPos -> return $ KickTo 2.2 TargetGoal
    _ | verycloseEnemy && inEnemyPenaltyArea g p && head toGoal == (p ^. property . idNumber) -> return (KickTo 2 TargetGoal)
    _ | inEnemyPenaltyArea g p && length (filter (p `isBehind`) (getEnemies g p)) <= 4 -> return $ KickTo 2.3 TargetGoal
    _ | inEnemyArea g p && length (filter (p `isBehind`) (getEnemies g p)) <= 5 -> return $ int
    _ | verycloseEnemy -> return $ KickTo 1.7 TargetOffsideLine
    _ | closeEnemy && inEnemyPenaltyArea g p && not aggresive && head toGoal == (p ^. property . idNumber) -> return $ KickTo 2.2 TargetGoal
    _ | closeEnemy && not aggresive -> return $ KickTo 2.2 TargetMate
    _ -> return int

--------------------------------------------------------------------------------
-- | Dribble to other place

--------------------------------------------------------------------------------
-- | Back to defence
newIntention _ (Just _) (Player _ (_roll -> PlayerKeeper) (GoFor _ DefenceLine)) = return $ KickTo 2.3 TargetMate
newIntention _ (Just _) (_intention -> GoFor _ DefenceLine) = return $ dribble TargetGoal

newIntention g Nothing p@(Player _ (PlayerProperty PlayerDefence _ _ i _) (GoFor _ DefenceLine)) = do
  let tc = getTeamContext g p
  let b@(Ball _ ballKeeper) = _ball g
  let t = max 1 $ expectedTime b (positionOf p) (p ^. geometrics_p . velocity)
  let nearToBall = (_idNumber . head . nearestPlayers (positionOf (b <!! t))) $ getTeammatePositions g p False
  let imDead = beyond g p TargetBall
  let ourBall = case ballKeeper of { Nothing -> False; Just x -> elem x (_teammates tc) }
  case () of
    _ | not ourBall && nearToBall == i && near2 160 (positionOf p) (positionOf b) -> return $ GoFor 0.7 TargetBall
    _ | nearToBall == i -> return $ GoFor 0.8 TargetBall
    _ | imDead -> return $ GoFor 0.8 DefenceLine
    _ -> return NoPlan

newIntention g Nothing p@(Player _ (PlayerProperty PlayerForward _ _ i _) (GoFor _ DefenceLine)) = do
  let tc = getTeamContext g p
  let b@(Ball _ ballKeeper) = _ball g
  let t = max 1 $ expectedTime b (positionOf p) (p ^. geometrics_p . velocity)
  let nearToBall = (_idNumber . head . nearestPlayers (positionOf (b <!! t))) $ getTeammatePositions g p False
  let ourBall = case ballKeeper of { Nothing -> False; Just x -> elem x (_teammates tc) }
  case () of
    _ | not ourBall && nearToBall == i && near2 140 (positionOf p) (positionOf b) -> return $ GoFor 0.7 TargetBall
    _ | nearToBall == i -> return $ GoFor 0.7 TargetBall
    _ -> return NoPlan

newIntention _ Nothing (_intention -> int@(GoFor _ _)) = return int

--------------------------------------------------------------------------------
-- | KickTo
newIntention _ Nothing (_intention -> KickTo _ _) = return $ Lost 4
newIntention _ _ (_intention -> int@(KickTo _ _)) = return int

--------------------------------------------------------------------------------
-- | final trap
newIntention _ b p@(_intention -> x) = putStr "newIntention: " >> error ("newIntention: " ++ show (b, x, p)) >> return x

--------------------------------------------------------------------------------
-------------------------------------------------------------------------------- updateGeometrics
--------------------------------------------------------------------------------

updateGeometrics:: GameContext -> Maybe Ball -> Player -> IO (Either (Geometrics, Ball) Geometrics)

--------------------------------------------------------------------------------
-- | Lost

updateGeometrics _ _ (Player pg _ (Lost _)) = do
  return . Right $ pg & velocity .~ 0

--------------------------------------------------------------------------------
-- | Go for
-- This is a blind behavior that does not depend on whether he has the ball or not.
-- Therefore the return value doesn't depend on the value of the 2nd argument basically.
updateGeometrics g Nothing p@(Player pg@(_position -> pos) (PlayerProperty PlayerKeeper _ _ i _) (GoFor _ TargetBall)) = do
  if (getTeamContext g p) ^. directorIntention == AggressiveAttack
    then do makeSpace <- moveToSpareSpace g p
            let (Geometrics _ dir v) = pg
            let d = movingSpeed
            let b = _ball g
            let t = max 1 $ expectedTime b pos (pg ^. velocity)
            let dir = directionTo pos $ (b <!! t) ^. (geometrics_b . position)
            let pg' = newGeometrics pos dir d
            return $ Right pg'
    else do let b = _ball g
            let t = max 1 $ expectedTime b pos 0
            let (dx, _) = directionTo pos $ (b <!! t) ^. (geometrics_b . position)
            return . Right $ newKeeperGeometrics (pg & direction .~ (signum dx, 0))

updateGeometrics g Nothing (Player pg@(_position -> pos) _ (GoFor d TargetBall)) = do
  let d = movingSpeed
  let b = _ball g
  let t = max 1 $ expectedTime b pos (pg ^. velocity)
  let dir = directionTo pos $ (b <!! t) ^. (geometrics_b . position)
  let pg' = newGeometrics pos dir d
  return $ Right pg'
  --let distStop = pos <-> positionOf b
  --let distMove = (pg ^. position) <-> positionOf b
  --return . Right $ if distStop < distMove then pg else pg'

updateGeometrics _ (Just _) (Player pg (PlayerProperty PlayerKeeper _ _ i _) (GoFor _ TargetBall)) = do
  return $ Left (pg, Ball pg (Just i))

updateGeometrics _ (Just _) (Player pg (_idNumber -> i) (GoFor _ TargetBall)) = do
  return $ Left (pg, Ball pg (Just i))

updateGeometrics g _ p@(Player (Geometrics (x, y) _ _) _ (GoFor v DefenceLine)) = do
  let v = movingSpeed
  let y' = currentEnemyOffside g p
  let dir@(dx', dy') = (0, signum $ y' - y)
  let newPos = keepInRange (x + dx' * v, y + dy' * v)
  return . Right $ Geometrics newPos dir v

updateGeometrics _ _ p@(Player pg _ (GoFor d TargetDefaultPosition)) = do
  let d = movingSpeed
  let defPos = p ^. property . defaultPosition
  let pos = pg ^. position
  let dist = pos <-> defPos
  if dist < 1
    then return . Right $ pg
    else do
        let dir = directionTo pos defPos -- Note: if pos == defPos, it returns NaN
        let v = if 3 * playerSize < dist then d else 0.1
        return . Right $ newGeometrics pos dir v

updateGeometrics g _ p@(Player pg@(_position -> pos) pp@(_idNumber -> i) (GoFor d TargetOffsideLine)) = do
  let d = movingSpeed
  let tc = getTeamContext g p
  let targetX = fhmargin + fwidth / 5 * if fst pos < fhmargin + fwidth / 2 then 1 else 4
  let targetY = fvmargin + fheight / 5 * if p ^. property . team == Team1 then 1 else 4
  let dir = directionTo (positionOf p) (targetX, targetY)
  return . Right $ newGeometrics pos dir d

updateGeometrics g _ p@(Player pg@(_position -> pos) pp@(_idNumber -> i) (GoFor d (TargetZone z))) = do
  let d = movingSpeed
  let dir = directionTo (positionOf p) (centerOfZone z)
  return . Right $ newGeometrics pos dir d

updateGeometrics g _ p@(Player pg@(_position -> pos) pp@(_idNumber -> i) (GoFor d (TargetGrid grid z))) = do
  let d = movingSpeed
  let dir = directionTo (positionOf p) (centerOfGrid grid z)
  return . Right $ newGeometrics pos dir d

updateGeometrics g _ p@(Player pg@(_position -> pos) pp@(_idNumber -> i) (GoFor d TargetGoal)) = do
  let d = movingSpeed
  let dir = directionTo (positionOf p) (getTeamContext g p ^. goalPosition)
  return . Right $ newGeometrics pos dir d

--------------------------------------------------------------------------------
-- | Kick
updateGeometrics _ Nothing (Player pg (_roll -> PlayerKeeper) (KickTo d _)) = do
  let d = kickSpeed
  return . Right $ newGeometrics (pg ^. position) (pg ^. direction) (pg ^. velocity)

updateGeometrics _ Nothing (Player pg _ (KickTo d _)) = do
  return . Right $ newGeometrics (pg ^. position) (pg ^. direction) (pg ^. velocity)

updateGeometrics g (Just _) p@(Player pg@(_position -> pos) (PlayerProperty PlayerKeeper _ _ i _) (KickTo d TargetGoal)) = do
  let d = 1.5 * kickSpeed
  let tc = getTeamContext g p
  let (tX', tY) = tc ^. goalPosition
  let tX = tX' + goalWidth / 4 * fromIntegral (mod (g ^. time) 5 - 2)
  let ballDir = randomizeDirection g (directionTo pos (tX, tY)) 5 i
  return $ Left (pg, Ball (Geometrics pos ballDir d) Nothing <!! 1)

updateGeometrics g (Just _) p@(Player pg@(_position -> (x, y)) pp@(PlayerProperty PlayerKeeper t _ i _) (KickTo d TargetMate)) = do
  let d = kickSpeed
  let tc = getTeamContext g p
  let aggresive = _directorIntention tc == AggressiveAttack
  let pg' = newGeometrics (x, y) (pg ^. direction) (pg ^. velocity)
  let newPos = pg' ^. position
  let p' =Player (pg & position .~ newPos) pp (Lost 3)
  let lst = (map snd . filter ((PlayerKeeper /=) . _roll . fst) . filter ((i /=) . _number . fst)) $ getTeammatePositions g p False
--  let enemysPos = filter ((< (if aggresive then 50 else 80)) . (<-> newPos)) . map snd . sortByDistance (x, y) $ getEnemyPositions g p
  let enemysPos = map snd $ getEnemyPositions g p
  -- let cands = foldr (\a b -> filter (not . coveredBy (if aggresive then 0.85 else 0.75) (x, y) a) b) lst enemysPos
  let cands = filter (not . covered 0.25 (x, y) enemysPos) lst
  let distanceTo (x1, y1) = sqrt $ (x1 -x) ** 2 + (y1 - y) ** 2
  let closerToGoal (_, y') = if t == Team1 then y' < y else y < y'
  let targetPos = case filter closerToGoal cands of
        [] | null cands -> maximumBy (comparing distanceTo) lst
        [] -> minimumBy (comparing distanceTo) lst
        l -> maximumBy (comparing distanceTo) l

  let ballDir = randomizeDirection g (directionTo (x, y) targetPos) 3 i
  let dist = distanceTo targetPos
  let d' = case () of
        _ | dist < 80 -> d - 0.1
        _ | dist < 200 -> d
        _ | dist < 300 -> d + 0.2
        _ -> d + 0.4
  return $ Left (p' ^. geometrics_p, Ball (Geometrics (x, y) ballDir d') Nothing <!! 1)

updateGeometrics g (Just _) p@(Player pg@(_position -> pos) pp@(_idNumber -> i) (KickTo d TargetGoal)) = do
  let d = 1.5 * kickSpeed
  let tc = getTeamContext g p
  let pg' = newGeometrics pos (pg ^. direction) (pg ^. velocity)
  let newPos = pg' ^. position
  let (tX', tY) = tc ^. goalPosition
  let tX = tX' + goalWidth / 5 * fromIntegral (mod (g ^. time) 5 - 2)
  let ballDir = randomizeDirection g (directionTo pos (tX, tY)) 5 i
  return $ Left (pg', Ball (Geometrics pos ballDir d) Nothing <!! 1)

updateGeometrics g (Just _) p@(Player pg@(_position -> (x, y)) pp@(_idNumber -> i) (KickTo d TargetOffsideLine)) = do
  let d = 1.3 * kickSpeed
  let tc = getTeamContext g p
  let pg' = newGeometrics (pg ^. position) (pg ^. direction) (pg ^. velocity)
  let es = map snd $ getEnemyPositions g p
  let view = take 3 $ if p ^. property . team == Team1 then viewOfTeam1 else viewOfTeam2
  let emptySpaceId = fst . minimumBy (comparing snd). zip [1 ..] $ map (\f -> length $ filter f es) view
  let targetX = fhmargin + fwidth / 4 * if p ^. property . team == Team1 then emptySpaceId else 4 - emptySpaceId
  let targetY = fvmargin + fheight / 5 * if p ^. property . team == Team1 then 1 else 4
  let ballDir = randomizeDirection g (directionTo (x, y) (targetX, targetY)) 5 i
  -- putStrLn $ "use side line:zone" ++ show emptySpaceId ++ " by " ++ show (p ^. property . team) ++ ", " ++ show (targetX, targetY)
  return $ Left (pg', Ball (Geometrics (x, y) ballDir d) Nothing <!! 1)

updateGeometrics g (Just l) p@(Player pg@(_position -> (x, y)) pp@(PlayerProperty _ t _ i _) (KickTo d TargetMate)) = do
  let d = kickSpeed
  let tc = getTeamContext g p
  let aggresive = _directorIntention tc == AggressiveAttack
  let pg' = newGeometrics (x, y) (pg ^. direction) (pg ^. velocity)
  let newPos = pg' ^. position
  let enemysPos = map snd . sortByDistance (x, y) $ getEnemyPositions g p
  let lst = (filter (not . near2 20 (x, y) . snd) . filter ((PlayerKeeper /=) . _roll . fst) . filter ((i /=) . _number . fst)) $ getTeammatePositions g p False
  let cands = filter (not . covered 0.25 (x, y) enemysPos . snd) lst
  let distanceTo (_, (x1, y1)) = sqrt $ (x1 -x) ** 2 + (y1 - y) ** 2
  let distY (_, (_, y1)) = abs (y1 - y)
  let closerToGoal (_, (_, y')) = if t == Team1 then y' < y else y < y'
  let enemyPenaltyArea = inEnemyPenaltyArea g p
  let ourArea = inOurArea g p
  let ourPenaltyArea = inOurPenaltyArea g p
  let ourArea = inOurArea g p
  let p@((_idNumber -> id), _) = case () of
        _ | not aggresive && enemyPenaltyArea ->
          case filter (not. closerToGoal) cands of
            [] | null cands -> minimumBy (comparing distanceTo) lst
            [] -> minimumBy (comparing distanceTo) cands
            l -> minimumBy (comparing distanceTo) l
        _ | aggresive || ourPenaltyArea ->
          case filter closerToGoal cands of
            [] | null cands -> minimumBy (comparing distanceTo) lst
            [] -> minimumBy (comparing distanceTo) cands
            l -> maximumBy (comparing distY) l
        _ | ourArea ->
          case filter closerToGoal cands of
            [] | null cands -> minimumBy (comparing distanceTo) lst
            [] -> minimumBy (comparing distanceTo) cands
            l -> maximumBy (comparing distanceTo) l
        _ ->
          case filter closerToGoal cands of
            [] -> maximumBy (comparing distanceTo) lst
            l -> maximumBy (comparing distanceTo) l
  let dist = distanceTo p
  let d' = case () of
        _ | dist < 80 -> d
        _ | dist < 200 -> d + 0.1
        _ | dist < 300 -> d + 0.3
        _ -> d + 0.6
  let p' = nthPlayer g id
  let dir1 = directionTo (x, y) (p' ^. geometrics_p . position)
  let targetPos = newPosition (p' ^. geometrics_p) 1
  let ballDir = randomizeDirection g (directionTo (x, y) targetPos) 5 i
  return $ Left (pg', Ball (Geometrics (x, y) ballDir d') Nothing <!! 1)

updateGeometrics g (Just _) p@(Player pg@(_position -> pos) pp@(_idNumber -> i) (KickTo d (TargetZone z))) = do
  let d = kickSpeed
  let pg' = newGeometrics pos (pg ^. direction) (pg ^. velocity)
  let ballDir = randomizeDirection g (directionTo pos (centerOfZone z)) 5 i
  return $ Left (pg', Ball (Geometrics pos ballDir d) Nothing <!! 1)

updateGeometrics g (Just _) p@(Player pg@(_position -> pos) pp@(_idNumber -> i) (KickTo d (TargetGrid grid z))) = do
  let d = kickSpeed
  let pg' = newGeometrics pos (pg ^. direction) (pg ^. velocity)
  let ballDir = randomizeDirection g (directionTo pos (centerOfGrid grid z)) 5 i
  return $ Left (pg', Ball (Geometrics pos ballDir d) Nothing <!! 1)

--------------------------------------------------------------------------------
-- Dribble
updateGeometrics g Nothing p@(Player pg@(Geometrics pos dir v) (_idNumber -> i) (DribblingTo d TargetGoal)) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (25, 50)
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let (dir', v') = case () of
        _ | verycloseEnemy && aggresive -> (randomizeDirection g dir 5 i, 0.9 * v)
        _ | verycloseEnemy -> (randomizeDirection g dir 5 i, 0.8 * v)
        _ | closeEnemy && aggresive -> (dir, v)
        _ | closeEnemy -> (randomizeDirection g dir 5 i, v)
        _ | aggresive -> ((directionTo pos (_goalPosition tc)), v)
        _ -> (randomizeDirection g (directionTo pos (_goalPosition tc)) 5 i, v)
  return $ Right $ newGeometrics pos dir' v'

updateGeometrics g (Just _) p@(Player pg@(Geometrics pos dir v) (_idNumber -> i) (DribblingTo d TargetGoal)) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (25, 50)
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let (dir', v') = case () of
        _ | verycloseEnemy && aggresive -> (randomizeDirection g dir 5 i, 0.9 * v)
        _ | verycloseEnemy -> (randomizeDirection g dir 5 i, 0.8 * v)
        _ | closeEnemy && aggresive -> (dir, v)
        _ | closeEnemy -> (randomizeDirection g dir 5 i, v)
        _ | aggresive -> ((directionTo pos (_goalPosition tc)), v)
        _ -> (randomizeDirection g (directionTo pos (_goalPosition tc)) 5 i, v)
  let pg' = newGeometrics pos dir' v'
  return $ Left (pg', Ball pg' (Just i))

updateGeometrics g (Just _) p@(Player pg@(Geometrics pos dir v) (_idNumber -> i) (DribblingTo d TargetOffsideLine)) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    targetX = fhmargin + fwidth / 5 * if fst pos < fhmargin + fwidth / 2 then 1 else 4
    targetY = fvmargin + fheight / 5 * if p ^. property . team == Team1 then 1 else 4
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (25, 50)
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let (dir', v') = case () of
        _ | verycloseEnemy && aggresive -> (randomizeDirection g dir 5 i, 0.9 * v)
        _ | verycloseEnemy -> (randomizeDirection g dir 5 i, 0.8 * v)
        _ | closeEnemy && aggresive -> (dir, v)
        _ | closeEnemy -> (randomizeDirection g dir 5 i, v)
        _ | aggresive -> ((directionTo pos (targetX, targetY)), v)
        _ -> (randomizeDirection g (directionTo pos (targetX, targetY)) 5 i, v)
  let pg' = newGeometrics pos dir' v'
  return $ Left (pg', Ball pg' (Just i))

updateGeometrics g Nothing p@(Player pg@(_position -> pos) (_idNumber -> i) (DribblingTo d TargetOffsideLine)) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    (zone1, zone2) = case _directorIntention tc of { AggressiveAttack -> (15, 25) ; _ -> (25, 50) }
    targetPos = p ^. property . defaultPosition
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let dir = case () of
        _ | verycloseEnemy -> randomizeDirection g (directionTo pos targetPos) 5 i
        _ | closeEnemy -> randomizeDirection g (directionTo pos targetPos) 5 i
        _ -> directionTo pos targetPos
  return . Right $ newGeometrics pos dir d

updateGeometrics g (Just _) p@(Player pg@(Geometrics pos _ v) (_idNumber -> i) (DribblingTo d DefenceLine)) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    y' = currentEnemyOffside g p
    dir = (0, signum $ y' - pos ^. _1)
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (25, 50)
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let (dir', v') = case () of
        _ | verycloseEnemy && aggresive -> (randomizeDirection g dir 5 i, 0.9 * v)
        _ | verycloseEnemy -> (randomizeDirection g dir 5 i, 0.8 * v)
        _ | closeEnemy && aggresive -> (dir, v)
        _ | closeEnemy -> (randomizeDirection g dir 5 i, v)
        _ | aggresive -> (directionTo pos (pos & _1 .~ y'), v)
        _ -> (randomizeDirection g (directionTo pos (pos & _1 .~ y')) 5 i, v)
  let pg' = newGeometrics pos dir' v'
  return $ Left (pg', Ball pg' (Just i))

updateGeometrics g (Just _) p@(Player pg@(Geometrics pos dir v) (_idNumber -> i) (DribblingTo d (TargetZone z))) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (25, 50)
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let (dir', v') = case () of
        _ | verycloseEnemy && aggresive -> (randomizeDirection g dir 5 i, 0.9 * v)
        _ | verycloseEnemy -> (randomizeDirection g dir 5 i, 0.8 * v)
        _ | closeEnemy && aggresive -> (dir, v)
        _ | closeEnemy -> (randomizeDirection g dir 5 i, v)
        _ | aggresive -> ((directionTo pos (centerOfZone z)), v)
        _ -> (randomizeDirection g (directionTo pos (centerOfZone z)) 5 i, v)
  let pg' = newGeometrics pos dir' v'
  return $ Left (pg', Ball pg' (Just i))

updateGeometrics g (Just _) p@(Player pg@(Geometrics pos dir v) (_idNumber -> i) (DribblingTo d (TargetGrid grid z))) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (25, 50)
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let (dir', v') = case () of
        _ | verycloseEnemy && aggresive -> (randomizeDirection g dir 5 i, 0.9 * v)
        _ | verycloseEnemy -> (randomizeDirection g dir 5 i, 0.8 * v)
        _ | closeEnemy && aggresive -> (dir, v)
        _ | closeEnemy -> (randomizeDirection g dir 5 i, v)
        _ | aggresive -> ((directionTo pos (centerOfGrid grid z)), v)
        _ -> (randomizeDirection g (directionTo pos (centerOfGrid grid z)) 5 i, v)
  let pg' = newGeometrics pos dir' v'
  return $ Left (pg', Ball pg' (Just i))

updateGeometrics g Nothing p@(Player pg@(Geometrics pos dir v) (_idNumber -> i) (DribblingTo d (TargetZone z))) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (25, 50)
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let (dir', v') = case () of
        _ | verycloseEnemy && aggresive -> (randomizeDirection g dir 5 i, 0.9 * v)
        _ | verycloseEnemy -> (randomizeDirection g dir 5 i, 0.8 * v)
        _ | closeEnemy && aggresive -> (dir, v)
        _ | closeEnemy -> (randomizeDirection g dir 5 i, v)
        _ | aggresive -> ((directionTo pos (centerOfZone z)), v)
        _ -> (randomizeDirection g (directionTo pos (centerOfZone z)) 5 i, v)
  return . Right $  newGeometrics pos dir' v'

updateGeometrics g Nothing p@(Player pg@(Geometrics pos dir v) (_idNumber -> i) (DribblingTo d (TargetGrid grid z))) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    aggresive = _directorIntention tc == AggressiveAttack
    (zone1, zone2) = if aggresive then (10, 20) else (25, 50)
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let (dir', v') = case () of
        _ | verycloseEnemy && aggresive -> (randomizeDirection g dir 5 i, 0.9 * v)
        _ | verycloseEnemy -> (randomizeDirection g dir 5 i, 0.8 * v)
        _ | closeEnemy && aggresive -> (dir, v)
        _ | closeEnemy -> (randomizeDirection g dir 5 i, v)
        _ | aggresive -> ((directionTo pos (centerOfGrid grid z)), v)
        _ -> (randomizeDirection g (directionTo pos (centerOfGrid grid z)) 5 i, v)
  return . Right $  newGeometrics pos dir' v'

updateGeometrics g Nothing p@(Player pg@(_position -> pos) (_idNumber -> i) (DribblingTo d TargetDefaultPosition)) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    (zone1, zone2) = case _directorIntention tc of { AggressiveAttack -> (15, 25) ; _ -> (25, 50) }
    targetPos = p ^. property . defaultPosition
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let dir = case () of
        _ | verycloseEnemy -> randomizeDirection g (directionTo pos targetPos) 5 i
        _ | closeEnemy -> randomizeDirection g (directionTo pos targetPos) 5 i
        _ -> directionTo pos targetPos
  return . Right $ newGeometrics pos dir d

updateGeometrics g (Just _) p@(Player pg@(_position -> pos) (_idNumber -> i) (DribblingTo d TargetDefaultPosition)) = do
  let d = dribbleSpeed
  let
    tc = getTeamContext g p
    (zone1, zone2) = case _directorIntention tc of { AggressiveAttack -> (15, 25) ; _ -> (25, 50) }
    targetPos = p ^. property . defaultPosition
  verycloseEnemy <- enemyIsNear g p zone1
  closeEnemy <- enemyIsNear g p zone2
  let dir = case () of
        _ | verycloseEnemy -> randomizeDirection g (directionTo pos targetPos) 5 i
        _ | closeEnemy -> randomizeDirection g (directionTo pos targetPos) 5 i
        _ -> directionTo pos targetPos
  let pg' = newGeometrics pos dir d
  return $ Left (pg', Ball pg' (Just i))

--------------------------------------------------------------------------------
-- | No plan
updateGeometrics g _ p@(Player pg@(Geometrics _ dir v) (_roll -> PlayerDefence) NoPlan) = do
  makeSpace <- moveToSpareSpace g p
  return . Right $ newGeometrics (pg ^. position) (case makeSpace of { Just d -> d; _ -> dir}) v

updateGeometrics g _ p@(Player pg@(Geometrics _ dir v) pp NoPlan) = do
  makeSpace <- moveToSpareSpace g p
  return . Right $ newGeometrics (pg ^. position) (case makeSpace of { Just d -> d; _ -> dir}) v

--------------------------------------------------------------------------------
-- | default trap for keeper

updateGeometrics g _ p@(Player pg@(_position-> pos) (_roll -> PlayerKeeper) _) = do
  if (getTeamContext g p) ^. directorIntention == AggressiveAttack
    then do makeSpace <- moveToSpareSpace g p
            let (Geometrics _ dir v) = pg
            return . Right $ newGeometrics (pg ^. position) (case makeSpace of { Just d -> d; _ -> dir}) v
    else do let b = _ball g
            let (dx', _) = directionTo pos (newPosition (geometricsOf b) 0)
            let pg' = pg & direction .~ (dx', 0)
            return . Right $ newKeeperGeometrics pg'

--------------------------------------------------------------------------------
-- | default trap
updateGeometrics _ b p@(_intention -> x) = putStr "updateGeometrics: " >> error (show (b, x, p)) >> return (Right (p ^. geometrics_p))

newKeeperGeometrics :: Geometrics -> Geometrics
newKeeperGeometrics pg@(Geometrics (x, y) (dx, _) v)
  | x' < centerX - band = pg
  | centerX + band < x' = pg
  | otherwise = pg'
  where
    pg' = newGeometrics (pg ^. position) (pg ^. direction) (pg ^. velocity)
    x' = pg' ^. (position . _1)
    centerX = fhmargin + fwidth / 2
    band = goalWidth / 2

inMyGoalZone :: Player -> Bool
inMyGoalZone p@(Player (_position -> (x, y)) _ _) =
  inX x && inY (p ^. property . team) y
  where
    inX :: Double -> Bool
    inX x = abs (x - fhmargin - fwidth / 2) < goalWidth / 2
    inY :: Team -> Double -> Bool
    inY Team1 y = abs (y - fvmargin) < goalWidth / 2
    inY Team2 y = abs (y - fvmargin - fheight) < goalWidth / 2
