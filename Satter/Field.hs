-- | arithmetics about field geometory
module Satter.Field
       (
         -- * Field Parameter
         fvmargin
       , fhmargin
       , fwidth
       , fheight
       , goalWidth
         -- * Field geometric computation
       , gotInGoal
       , positionLiterals
       , viewOfTeam1
       , viewOfTeam2
         -- * zone
       , centerOfZone
         -- * predicates
       , outOfField
       , inLeftOfTeam1PenaltyArea
       , inTeam1PenaltyArea
       , inRightOfTeam1PenaltyArea
       , inLeftOfTeam1Area
       , inCenterOfTeam1Area
       , inRightOfTeam1Area
       , inLeftOfTeam2PenaltyArea
       , inTeam2PenaltyArea
       , inRightOfTeam2PenaltyArea
       , inLeftOfTeam2Area
       , inCenterOfTeam2Area
       , inRightOfTeam2Area
         -- * grid
       , centerOfGrid
       , inGrid
       , gridViewOfTeam1
       , gridViewOfTeam2
       )
       where

import Satter.TwoD

gotInGoal :: TwoD -> TwoD -> Maybe Int
gotInGoal (ox, oy) (nx, ny)
  | ny < fvmargin && fvmargin < oy && inGoalZone (imaginaryX 1) = Just 1
  | fvmargin + fheight < ny && oy < fvmargin + fheight && inGoalZone (imaginaryX 2) = Just 2
  | otherwise = Nothing
  where
    imaginaryX 1 = (nx - ox) / (ny - oy) * (fvmargin - oy) + ox
    imaginaryX 2 = (nx - ox) / (ny - oy) * (fvmargin + fheight - oy) + ox
    inGoalZone x = abs (x - fhmargin - fwidth / 2) < goalWidth / 2

fvmargin :: Double
fvmargin = 24

fhmargin :: Double
fhmargin = 4

fwidth :: Double
fwidth = 480.0

fheight :: Double
fheight = 800.0

goalWidth :: Double
goalWidth = 100

outOfField :: TwoD -> Bool
outOfField (x, y) = x < fhmargin || fhmargin + fwidth < x || y < fvmargin || fvmargin + fheight < y

--------------------------------------------------------------------------------
centerOfZone :: Int -> TwoD
centerOfZone 1 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + 0
    right = fhmargin + fwidth /3
    top = fvmargin + 0
    bottom = fvmargin + fheight / 4

centerOfZone 2 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + fwidth / 3
    right = fhmargin + fwidth / 3 * 2
    top = fvmargin + 0
    bottom = fvmargin + fheight / 4

centerOfZone 3 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + fwidth / 3 * 2
    right = fhmargin + fwidth
    top = fvmargin + 0
    bottom = fvmargin + fheight / 4

centerOfZone 4 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + 0
    right = fhmargin + fwidth / 3
    top = fvmargin + fheight / 4
    bottom = fvmargin + fheight / 4 * 2

centerOfZone 5 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + fwidth / 3
    right = fhmargin + fwidth / 3 * 2
    top = fvmargin + fheight / 4
    bottom = fvmargin + fheight / 4 * 2

centerOfZone 6 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + fwidth / 3 * 2
    right = fhmargin + fwidth
    top = fvmargin + fheight / 4
    bottom = fvmargin + fheight / 4 * 2

centerOfZone 7 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin
    right = fhmargin + fwidth / 3
    top = fvmargin + fheight / 4 * 2
    bottom = fvmargin + fheight / 4 * 3

centerOfZone 8 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + fwidth / 3
    right = fhmargin + fwidth / 3 * 2
    top = fvmargin + fheight / 4 * 2
    bottom = fvmargin + fheight / 4 * 3

centerOfZone 9 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + fwidth / 3 * 2
    right = fhmargin + fwidth
    top = fvmargin + fheight / 4 * 2
    bottom = fvmargin + fheight / 4 * 3

centerOfZone 10 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin
    right = fhmargin + fwidth / 3
    top = fvmargin + fheight / 4 * 3
    bottom = fvmargin + fheight

centerOfZone 11 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + fwidth / 3
    right = fhmargin + fwidth / 3 * 2
    top = fvmargin + fheight / 4 * 3
    bottom = fvmargin + fheight

centerOfZone 12 = ((left + right) / 2, (top + bottom) / 2)
  where
    left = fhmargin + fwidth / 3 * 2
    right = fhmargin + fwidth
    top = fvmargin + fheight / 4 * 3
    bottom = fvmargin + fheight

--------------------------------------------------------------------------------
-- | zone 1
inLeftOfTeam2PenaltyArea :: TwoD -> Bool
inLeftOfTeam2PenaltyArea (x, y) = (left <= x && x <= right) && (top <= y && y < bottom)
  where
    left = fhmargin + 0
    right = fhmargin + fwidth /3
    top = fvmargin + 0
    bottom = fvmargin + fheight / 4

-- | zene 2
inTeam2PenaltyArea :: TwoD -> Bool
inTeam2PenaltyArea (x, y) = (left < x && x < right) && (top <= y && y < bottom)
  where
    left = fhmargin + fwidth / 3
    right = fhmargin + fwidth / 3 * 2
    top = fvmargin + 0
    bottom = fvmargin + fheight / 4

-- | zene 3
inRightOfTeam2PenaltyArea :: TwoD -> Bool
inRightOfTeam2PenaltyArea (x, y) = (left <= x && x <= right) && (top <= y && y < bottom)
  where
    left = fhmargin + fwidth / 3 * 2
    right = fhmargin + fwidth
    top = fvmargin + 0
    bottom = fvmargin + fheight / 4

-- | zene 4
inLeftOfTeam2Area :: TwoD -> Bool
inLeftOfTeam2Area (x, y) = (left <= x && x <= right) && (top <= y && y <= bottom)
  where
    left = fhmargin + 0
    right = fhmargin + fwidth / 3
    top = fvmargin + fheight / 4
    bottom = fvmargin + fheight / 4 * 2

-- | zene 5
inCenterOfTeam2Area :: TwoD -> Bool
inCenterOfTeam2Area (x, y) = (left < x && x < right) && (top <= y && y < bottom)
  where
    left = fhmargin + fwidth / 3
    right = fhmargin + fwidth / 3 * 2
    top = fvmargin + fheight / 4
    bottom = fvmargin + fheight / 4 * 2

-- | zene 6
inRightOfTeam2Area :: TwoD -> Bool
inRightOfTeam2Area (x, y) = (left <= x && x <= right) && (top <= y && y <= bottom)
  where
    left = fhmargin + fwidth / 3 * 2
    right = fhmargin + fwidth
    top = fvmargin + fheight / 4
    bottom = fvmargin + fheight / 4 * 2

-- | zene 7
inLeftOfTeam1Area :: TwoD -> Bool
inLeftOfTeam1Area (x, y) = (left <= x && x <= right) && (top < y && y <= bottom)
  where
    left = fhmargin
    right = fhmargin + fwidth / 3
    top = fvmargin + fheight / 4 * 2
    bottom = fvmargin + fheight / 4 * 3

-- | zene 8
inCenterOfTeam1Area :: TwoD -> Bool
inCenterOfTeam1Area (x, y) = (left < x && x < right) && (top <= y && y <= bottom)
  where
    left = fhmargin + fwidth / 3
    right = fhmargin + fwidth / 3 * 2
    top = fvmargin + fheight / 4 * 2
    bottom = fvmargin + fheight / 4 * 3

-- | zene 9
inRightOfTeam1Area :: TwoD -> Bool
inRightOfTeam1Area (x, y) = (left <= x && x <= right) && (top < y && y <= bottom)
  where
    left = fhmargin + fwidth / 3 * 2
    right = fhmargin + fwidth
    top = fvmargin + fheight / 4 * 2
    bottom = fvmargin + fheight / 4 * 3

-- | zene 10
inLeftOfTeam1PenaltyArea :: TwoD -> Bool
inLeftOfTeam1PenaltyArea (x, y) = (left <= x && x <= right) && (top < y && y <= bottom)
  where
    left = fhmargin
    right = fhmargin + fwidth / 3
    top = fvmargin + fheight / 4 * 3
    bottom = fvmargin + fheight

-- | zene 11
inTeam1PenaltyArea :: TwoD -> Bool
inTeam1PenaltyArea (x, y) = (left < x && x < right) && (top < y && y <= bottom)
  where
    left = fhmargin + fwidth / 3
    right = fhmargin + fwidth / 3 * 2
    top = fvmargin + fheight / 4 * 3
    bottom = fvmargin + fheight

-- | zene 12
inRightOfTeam1PenaltyArea :: TwoD -> Bool
inRightOfTeam1PenaltyArea (x, y) = (left <= x && x <= right) && (top < y && y <= bottom)
  where
    left = fhmargin + fwidth / 3 * 2
    right = fhmargin + fwidth
    top = fvmargin + fheight / 4 * 3
    bottom = fvmargin + fheight

-- | return [Bool]
positionLiterals :: [TwoD-> Bool] -> TwoD -> [Bool]
positionLiterals fs pos = map ($ pos) fs


-- |
viewOfTeam1 :: [TwoD -> Bool]
viewOfTeam1 =
  [
    inLeftOfTeam2PenaltyArea
  , inTeam2PenaltyArea
  , inRightOfTeam2PenaltyArea
  , inLeftOfTeam2Area
  , inCenterOfTeam2Area
  , inRightOfTeam2Area
  , inLeftOfTeam1Area
  , inCenterOfTeam1Area
  , inRightOfTeam1Area
  , inLeftOfTeam1PenaltyArea
  , inTeam1PenaltyArea
  , inRightOfTeam1PenaltyArea
  ]

viewOfTeam2 :: [TwoD -> Bool]
viewOfTeam2 =
  [
    inRightOfTeam1PenaltyArea
  , inTeam1PenaltyArea
  , inLeftOfTeam1PenaltyArea
  , inRightOfTeam1Area
  , inCenterOfTeam1Area
  , inLeftOfTeam1Area
  , inRightOfTeam2Area
  , inCenterOfTeam2Area
  , inLeftOfTeam2Area
  , inRightOfTeam2PenaltyArea
  , inTeam2PenaltyArea
  , inLeftOfTeam2PenaltyArea
  ]

--------------------------------------------------------------------------------
-- grid of field

-- | grid is 1-based indexed region seaquece to disgnate a region of the field
--
-- >>> centerOfGrid (1, 1) 1
-- (244,0,424.0)    -- (480 / 2 + 4, 800 / 2 + 24)
--
-- >>> centerOfGrid (3,3) 5
-- (244,0,424.0)    -- (480 / 3 * 1.5 + 4, 800 / 3 * 1.5 + 24)
--
-- >>> centerOfGrid (10,10) 100
-- (460.0,784.0)    -- (480 / 10 * 9.5 + 4, 800 / 10 * 9.5 + 24)
--
centerOfGrid :: (Int, Int) -> Int -> TwoD
centerOfGrid (c, r) i
  | i < 1 = error $ "out of index for grid, too small:" ++ show i
  | c * r < 1 = error $ "out of index for grid, too large: " ++ show i
  | otherwise = (x + fhmargin, y + fvmargin)
  where
    ix = mod (i - 1) c
    iy = div (i - 1) c
    x = fwidth / fromIntegral c * (fromIntegral ix + 0.5)
    y = fheight / fromIntegral r * (fromIntegral iy + 0.5)

-- | region check
--
-- >>> inGrid (1,1) 1 (fhmargin + fwidth, fvmargin + fheight)
-- True
--
-- >>> inGrid (1,1) 1 (fhmargin + fwidth, fvmargin + fheight + 1)
-- False
--
-- >>> inGrid (5,5) 13 (fhmargin + fwidth / 2, fvmargin + fheight / 2)
--
--
inGrid :: (Int, Int) -> Int -> TwoD -> Bool
inGrid (c, r) i (x, y) = between bx width x && between by height y
  where
    ix = mod (i - 1) c
    iy = div (i - 1) c
    width = fwidth / fromIntegral c
    height = fheight / fromIntegral r
    bx = fhmargin + width * fromIntegral ix
    by = fvmargin + height * fromIntegral iy
    between p step x = p <= x && x <= p + step

gridViewOfTeam1 :: (Int, Int) -> [TwoD -> Bool]
gridViewOfTeam1 (c, r) = [inGrid (c, r) i | i <- [1 .. c * r]]

gridViewOfTeam2 :: (Int, Int) -> [TwoD -> Bool]
gridViewOfTeam2 (c, r) = [inGrid (c, r) i | i <- [c * r, c * r - 1 .. 1]]
